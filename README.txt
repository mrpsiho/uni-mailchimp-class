Uni Mailchimp Class

version 0.1.0

This class is designed to connect WP powered websites with a new MailChimp API 3.0. A lot of things are changed in the new version of the API compared to version 2.0.

Important: this class uses WP functions and won't work without WordPress!